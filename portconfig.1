.\"
.\" Copyright (c) 2022-2025 Alfonso Sabato Siciliano
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.Dd January 7, 2025
.Dt PORTCONFIG 1
.Os
.Sh NAME
.Nm portconfig
.Nd port options
.Sh SYNOPSIS
.Nm portconfig
.Op Fl h | v
.Sh DESCRIPTION
The
.Nm
utility provides a TUI to customize the options to build a port.
The following options are available:
.Bl -tag -width indent
.It Fl h
Display options, a short description and exit.
.It Fl v
Show version and exit.
.El
.Pp
The following user interface features are available:
.Bl -tag -width indent
.It F1 key
General PORTS usage.
.It Help button
Specific port help, if
.Pa pkg-help
exists.
.It Space key
Select/deselect an option.
.El
.Sh ENVIRONMENT
The following environment variables can customize
.Nm portconfig :
.Bl -tag -width indent
.It Ev NO_COLOR=<something>
Disable colors if present and not an empty string (regardless of its value).
.It Ev PORTCONFIG_ALIGNCENTER=YES
Align options to center, default left.
.It Ev PORTCONFIG_ASCIILINES=YES
Ascii characters to draw lines.
.It Ev PORTCONFIG_CLOCALE=YES
Force using only 8 bit characters, useful in non-utf-8 environments.
.It Ev PORTCONFIG_FULLSCREEN=YES
Fullscreen mode.
.It Ev PORTCONFIG_HEIGHT=<height>
Fixed height, default autosize.
.It Ev PORTCONFIG_MINHEIGHT=<height>
Minimum height, takes effect only with autosize.
.It Ev PORTCONFIG_MINWIDTH=<width>
Minimum width, takes effect only with autosize.
.It Ev PORTCONFIG_NOLINES=YES
Do not draw lines.
.It Ev PORTCONFIG_NOSHADOW=YES
Disable dialog shadow.
.It Ev PORTCONFIG_THEMEFILE=<file>
Load theme file.
.It Ev PORTCONFIG_WIDTH=<width>
Fixed width, default autosize.
.El
.Pp
See
.Sx EXAMPLES
section below.
.Ss Compatibility
The following environment variables are available for compatibility:
.Bd -literal -offset indent -compact
D4PALIGNCENTER        PORTCONFIG_ALIGNCENTER
D4PASCIILINES         PORTCONFIG_ASCIILINES
D4PFULLSCREEN         PORTCONFIG_FULLSCREEN
D4PHEIGHT             PORTCONFIG_HEIGHT
D4PMINHEIGHT          PORTCONFIG_MINHEIGHT
D4PWIDTH              PORTCONFIG_WIDTH
.Ed
.Sh FILES
The theme file
.Pa /usr/local/etc/portconfig/theme.conf
is read on startup if exists.
.Pp
Template theme files are installed in
.Pa /usr/local/etc/portconfig/ .
.Pp
To create a new theme file run:
.Dl /usr/local/bin/bsddialog --save-theme mytheme.conf --infobox \e \
\*qSaving theme...\*q 0 0
.Sh THEME
The theme configuration in order of priority is as follows:
.Bl -tag -width indent
.It Check Terminal Colors.
.Nm portconfig
checks if the environments has colors, otherwise sets black and white theme.
.It Ev env NO_COLOR=<something>
If present and not an empty string sets black and white theme.
.It Ev env PORTCONFIG_THEMEFILE=<file>
if <file> exists and is well-formed its theme is set.
Otherwise the default theme is set.
.It Pa /usr/local/etc/portconfig/theme.conf
if the file exists and is well-formed its theme is set.
Otherwise the default theme is set.
.It Otherwise
Default theme file set.
.El
.Pp
These options are mutually exclusive.
.Sh EXIT STATUS
.Ex -std
.Sh EXAMPLES
Examples refer to a standard installation with
.Xr sh 1
and
.Xr tcsh 1
calling
.Nm portconfig
implicitly via
.Dq make config
in
.Pa /usr/ports/<category>/<port> .
.Pp
To run
.Nm
in black and white:
.Dl env NO_COLOR=YES make config
.Pp
Add the following line to
.Xr make.conf 5
to make this setting permanent:
.Dl export NO_COLOR=YES
.Sh SEE ALSO
.Xr portoptscli 1 ,
.Xr ports 7
.Sh AUTHORS
The
.Nm
utility was written by
.An Alfonso Sabato Siciliano
.Aq Mt asiciliano@FreeBSD.org .
.Sh THANKS TO
.An Baptiste Daroussin
.Aq Mt bapt@FreeBSD.org
for suggestions, help, and testing.
