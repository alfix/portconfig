# PortConfig 0.6.2

The [FreeBSD](https://www.freebsd.org) Operating System provides the
[Ports Collection](https://www.freebsd.org/ports/) to give users and
administrators a simple way to install applications.
It is possible to configure a port before the building and installation.
PortConfig is an utility for setting the port options via a Text User Interface.


## Getting Started

To install the port
[ports-mgmt/portconfig](https://www.freshports.org/ports-mgmt/portconfig/):

	# cd /usr/ports/ports-mgmt/portconfig/ && make install clean

To add the package:

	# pkg install portconfig

Add to */etc/make.conf*:

```Makefile
DIALOG4PORTS=${LOCALBASE}/bin/portconfig
```


## Documentation

Manual, after installation:

	% man 1 portconfig

online:
[blog post](https://alfonsosiciliano.gitlab.io/posts/2022-01-06-manual-portconfig.html)
or
[man.freebsd.org](https://man.freebsd.org/cgi/man.cgi?query=portconfig&manpath=FreeBSD+14.2-RELEASE+and+Ports)

## Screenshots

Preview 1:

	# cd /usr/ports/editor/vim
	# make config

![screenshot default theme](images/screenshot1.png)

Preview 2:

	# cd /usr/ports/editor/vim
	# env NO_COLOR=Y make config

![screenshot dialog theme](images/screenshot2.png)



## TODO and Ideas

 - [Problems and Feature Requests](https://wiki.freebsd.org/portconfig).
 - OPTIONS\_NAME\_ORDER.
 - wait and use 'bsddialog\_menugroup.min\_on = 1' for OPTIONS\_MULTI and
   OPTIONS\_SINGLE.


## Dev Notes

Coding:

```
% git clone https://gitlab.com/alfix/portconfig.git
% cd portconfig
% git clone https://gitlab.com/alfix/bsddialog.git
% cd bsddialog
% make
% cd ..
% make -DDEVELOPMENT
```

Tip, to check empty string name before adding the option to the menu.


Ports framework important files:

*/usr/ports/Mk/bsd.commands.mk* for command path:

```Makefile
#DIALOG4PORTS?= ${LOCALBASE}/bin/dialog4ports
DIALOG4PORTS?= /absolute/path/to/portconfig
```

*/usr/ports/Mk/Scripts/dialog4ports.sh* for *-v* option: portconfig has to
provide *-v*, so the ports framework waits stderr (instead of stdout).

*/usr/ports/Mk/bsd.port.mk* for env port options:

```Makefile
.if !target(pre-config)
pre-config:
D4P_ENV=	PKGNAME="${PKGNAME}" \
		PORT_OPTIONS="${PORT_OPTIONS}" \
		ALL_OPTIONS="${ALL_OPTIONS}" \
		OPTIONS_MULTI="${OPTIONS_MULTI}" \
		OPTIONS_SINGLE="${OPTIONS_SINGLE}" \
		OPTIONS_RADIO="${OPTIONS_RADIO}" \
		OPTIONS_GROUP="${OPTIONS_GROUP}" \
		NEW_OPTIONS="${NEW_OPTIONS}" \
		DIALOG4PORTS="${DIALOG4PORTS}" \
		PREFIX="${PREFIX}" \
		LOCALBASE="${LOCALBASE}" \
		PORTSDIR="${PORTSDIR}" \
		MAKE="${MAKE}" \
		D4PHEIGHT="${D4PHEIGHT}" \
		D4PMINHEIGHT="${D4PMINHEIGHT}" \
		D4PWIDTH="${D4PWIDTH}" \
		D4PFULLSCREEN="${D4PFULLSCREEN}" \
		D4PALIGNCENTER="${D4PALIGNCENTER}" \
		D4PASCIILINES="${D4PASCIILINES}"
.if exists(${PKGHELP})
D4P_ENV+=	PKGHELP="${PKGHELP}"
.endif
.for opt in ${ALL_OPTIONS}
D4P_ENV+=	 ${opt}_DESC=""${${opt}_DESC:Q}""
.endfor
.for otype in MULTI GROUP SINGLE RADIO
.  for m in ${OPTIONS_${otype}}
D4P_ENV+=	OPTIONS_${otype}_${m}="${OPTIONS_${otype}_${m}}" \
		${m}_DESC=""${${m}_DESC:Q}""
.    for opt in ${OPTIONS_${otype}_${m}}
D4P_ENV+=	 ${opt}_DESC=""${${opt}_DESC:Q}""
.    endfor
.  endfor
.endfor
.undef m
.undef otype
.undef opt
.endif # pre-config
```

