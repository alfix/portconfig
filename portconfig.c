/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2021-2025 Alfonso Sabato Siciliano
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <err.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <bsddialog.h>
#include <bsddialog_theme.h>

#include "theme.h"

#define VERSION     "0.6.2"

#define EMPTYSTR    "" /* no prefix, name, desc, bottomdesc */
#define F1FILE      "/usr/ports/README"
#define F1MSG       F1FILE " does not exist, please run:\n\n  $ man 7 ports\n"
#define MANDATORY   "[select at least one] "
#define NEWPREFIX   "new"
#define TEXT        "'F1' for Ports Collection help."
#define TEXTHELP    "'Help' button for port info, " TEXT

#ifdef DEBUG
#define DEBUGPRINTF(...) printf(__VA_ARGS__)
#else
#define DEBUGPRINTF(...) /**/
#endif

static char *new_options;
static char *on_options;

static char* _strdup(char *string)
{
	char *allocated;

	if ((allocated = strdup(string)) == NULL)
		err(EXIT_FAILURE, "Cannot allocate \"%s\"", string);

	return (allocated);
}

static bool _strstr(char *big, char *little)
{
	bool matched;
	char *sub, *tmp, *tofree;

	tofree = tmp = _strdup(big);
	matched = false;
	while ((sub = strsep(&tmp, "\t\n\v\f\r ")) != NULL) {
		if (strcmp(little, sub) == 0) {
			matched = true;
			break;
		}
	}
	free(tofree);

	return (matched);
}

static void
addsinglegroup(char *envname, enum bsddialog_menutype type,
    unsigned int *ngroups, struct bsddialog_menugroup **groups)
{
	struct bsddialog_menuitem *item = NULL;
	unsigned int nitem;
	char buf[1024], *desc, *ev, *name, *string, *tofree;

	ev = getenv(envname);
	if(ev == NULL || strlen(ev) < 1)
		return;

	DEBUGPRINTF("{M} %s: %s\n", envname, ev);
	nitem = 0;
	tofree = string = _strdup(ev);
	while ((name = strsep(&string, "\t\n\v\f\r ")) != NULL) {
		if (strlen(name) < 1)
			continue;

		item = realloc(item,
		    (nitem + 1) * sizeof(struct bsddialog_menuitem));
		if (item == NULL)
			err(EXIT_FAILURE, "Cannot realloc for item %s", name);
		item[nitem].prefix = _strstr(new_options, name) ?
		    NEWPREFIX : EMPTYSTR;
		item[nitem].on = _strstr(on_options, name) ? true : false;
		item[nitem].depth = 0;
		item[nitem].name = _strdup(name);
		sprintf(buf, "%s_DESC", name);
		desc = getenv(buf);
		desc = (desc != NULL && strlen(desc) > 0) ? desc : EMPTYSTR;
		item[nitem].desc = desc;
		item[nitem].bottomdesc = EMPTYSTR;
		DEBUGPRINTF("{I} %s - [%s]: %s\n", name, buf, desc);

		nitem++;
	}

	if (nitem > 0) {
		*groups = realloc(*groups,
		    (*ngroups + 1) * sizeof(struct bsddialog_menugroup));
		if (groups == NULL)
			err(EXIT_FAILURE, "Cannot realloc %s group", envname);
		(*groups)[*ngroups].type = type;
		(*groups)[*ngroups].nitems = nitem;
		(*groups)[*ngroups].items = item;
		*ngroups = *ngroups + 1;
	}

	free(tofree);
}

static void
addmultigroup(char *envname, enum bsddialog_menutype type, bool mandatory,
    unsigned int *ngroups, struct bsddialog_menugroup **groups)
{
	char buf[1024], *desc, *ev, *name, *string, *tofree;
	struct bsddialog_menuitem *item;

	ev = getenv(envname);
	if(ev == NULL || strlen(ev) < 1)
		return;

	DEBUGPRINTF("--------------------------------\n");
	DEBUGPRINTF("** [%s]: %s **\n", envname, ev);
	tofree = string = _strdup(ev);
	while ((name = strsep(&string, "\t\n\v\f\r ")) != NULL) {
		/* separator */
		if ((item = malloc(sizeof(struct bsddialog_menuitem))) == NULL)
			err(EXIT_FAILURE, "Cannot allocate sep for %s", name);
		item->prefix = EMPTYSTR;
		item->on = false;
		item->depth = 0;
		sprintf(buf, "%s_DESC", name);
		desc = getenv(buf);
		desc = (desc != NULL && strlen(desc) > 0) ? desc : name;
		asprintf(__DECONST(char**, &item->name), " %s%c",
		    desc, mandatory ? '\0' : ' ');
		item->desc = mandatory ? MANDATORY : EMPTYSTR;
		item->bottomdesc = EMPTYSTR;
		DEBUGPRINTF("{S} --(%s) [%s]: %s--\n", name, buf, desc);

		*groups = realloc(*groups,
		    (*ngroups + 1) * sizeof(struct bsddialog_menugroup));
		if (groups == NULL)
			err(EXIT_FAILURE, "Cannot realloc %s group", envname);
		(*groups)[*ngroups].type = BSDDIALOG_SEPARATOR;
		(*groups)[*ngroups].nitems = 1;
		(*groups)[*ngroups].items = item;
		*ngroups = *ngroups + 1;

		/* real menu */
		sprintf(buf, "%s_%s", envname, name);
		addsinglegroup(buf, type, ngroups, groups);
	}

	free(tofree);
}

static void usage(void)
{
	fprintf(stderr, "usage: portconfig [-h | -v]\n");
	fprintf(stderr, " -h  Display this help\n");
	fprintf(stderr, " -v  Print version\n");
}

int main(int argc, char *argv[])
{
	struct bsddialog_conf conf, confhelp;
	struct bsddialog_menugroup *groups;
	int ch, h, w, output, focuslist, focusitem;
	unsigned int i, j, ngroups;
	char *env, *helpfile, *text;

	/* Experimental new env to switch locale: UTF-8 or C */
	if ((env = getenv("PORTCONFIG_CLOCALE")) == NULL)
		setlocale(LC_ALL, "");
	else if (strcasecmp(env, "Y") != 0 && strcasecmp(env, "YES") != 0)
		setlocale(LC_ALL, "");

	bsddialog_initconf(&conf);
	conf.menu.align_left = true;
	conf.key.enable_esc = true;
	conf.auto_topmargin = 1;
	conf.auto_downmargin = 1;
	h = BSDDIALOG_AUTOSIZE;
	w = BSDDIALOG_AUTOSIZE;
	text = TEXT;
	focuslist = -1;
	focusitem = -1;
	ngroups = 0;
	groups = NULL;
	if (access(F1FILE, F_OK) == 0)
		conf.key.f1_file = F1FILE;
	else
		conf.key.f1_message = F1MSG;

	/* cli */
	while ((ch = getopt(argc, argv, "hv")) != -1) {
		switch (ch) {
		case 'v':
			fprintf(stderr, "portconfig version: %s "
			    "(libbsddialog: %s).\n",
			    VERSION, LIBBSDDIALOG_VERSION);
			return (EXIT_SUCCESS);
		case 'h':
			usage();
			fprintf(stderr, "\nPress F1 inside portconfig "
			    "for Ports Collection help.\n");
			fprintf(stderr, "See \'man portconfig\' "
			    "for more information.\n");
			return (EXIT_SUCCESS);
		default:
			usage();
			return (EXIT_FAILURE);
		}
	}
	argc -= optind;
	argv += optind;

	/* internal envs */
	if ((env = getenv("PKGNAME")) == NULL) {
		fprintf(stderr, "Error: cannot get port name from env "
		    "PKGNAME\n");
		return (EXIT_FAILURE);
	}
	conf.title = env;

	if (((env = getenv("PKGHELP")) != NULL) && (access(env, F_OK) == 0)) {
		DEBUGPRINTF("[PKGHELP]: %s\n", env);
		helpfile = env;
		text = TEXTHELP;
		conf.button.with_help = true;
	}

	/* public envs */
	if ((env = getenv("PORTCONFIG_NOSHADOW")) != NULL)
		if (strcasecmp(env, "Y") == 0 || strcasecmp(env, "YES") == 0)
			conf.shadow = false;

	if ((env = getenv("PORTCONFIG_HEIGHT")) != NULL ||
	    (env = getenv("D4PHEIGHT")) != NULL)
		h = (int)strtol(env, NULL, 0);

	if ((env = getenv("PORTCONFIG_MINHEIGHT")) != NULL ||
	    (env = getenv("D4PMINHEIGHT")) != NULL)
		conf.auto_minheight = (unsigned int)strtoul(env, NULL, 0);

	if ((env = getenv("PORTCONFIG_WIDTH")) != NULL ||
	    (env = getenv("D4PWIDTH")) != NULL)
		w = (int)strtol(env, NULL, 0);

	if ((env = getenv("PORTCONFIG_MINWIDTH")) != NULL)
		conf.auto_minwidth = (unsigned int)strtoul(env, NULL, 0);

	if ((env = getenv("PORTCONFIG_FULLSCREEN")) != NULL ||
	    (env = getenv("D4PFULLSCREEN")) != NULL)
		if (strcasecmp(env, "Y") == 0 || strcasecmp(env, "YES") == 0)
			h = w = BSDDIALOG_FULLSCREEN;

	if ((env = getenv("PORTCONFIG_ALIGNCENTER")) != NULL ||
	    (env = getenv("D4PALIGNCENTER")) != NULL)
		if (strcasecmp(env, "Y") == 0 || strcasecmp(env, "YES") == 0)
			conf.menu.align_left = false;

	if ((env = getenv("PORTCONFIG_ASCIILINES")) != NULL ||
	    (env = getenv("D4PASCIILINES")) != NULL)
		if (strcasecmp(env, "Y") == 0 || strcasecmp(env, "YES") == 0)
			conf.ascii_lines = true;

	if ((env = getenv("PORTCONFIG_NOLINES")) != NULL)
		if (strcasecmp(env, "Y") == 0 || strcasecmp(env, "YES") == 0)
			conf.no_lines = true;

	/* mixedlist building */
	new_options = getenv("NEW_OPTIONS");
	DEBUGPRINTF("[NEW_OPTIONS]: %s\n", new_options);
	on_options = getenv("PORT_OPTIONS");
	DEBUGPRINTF("[PORT_OPTIONS] (on opts): %s\n", on_options);

	DEBUGPRINTF("** [ALL_OPTIONS] (if exist under) **\n");
	addsinglegroup("ALL_OPTIONS", BSDDIALOG_CHECKLIST, &ngroups, &groups);
	addmultigroup("OPTIONS_GROUP", BSDDIALOG_CHECKLIST, false, &ngroups,
	    &groups);
	addmultigroup("OPTIONS_MULTI", BSDDIALOG_CHECKLIST, true, &ngroups,
	    &groups);
	addmultigroup("OPTIONS_SINGLE", BSDDIALOG_RADIOLIST, true, &ngroups,
	    &groups);
	addmultigroup("OPTIONS_RADIO", BSDDIALOG_RADIOLIST, false, &ngroups,
	    &groups);

	/* text user interface */
	if (set_theme() == BSDDIALOG_ERROR)
		fprintf(stderr, "Error: %s\n", bsddialog_geterror());

	memcpy(&confhelp, &conf, sizeof(struct bsddialog_conf));
	confhelp.y = BSDDIALOG_CENTER;
	confhelp.x = BSDDIALOG_CENTER;
	confhelp.auto_topmargin = 0;
	confhelp.auto_downmargin = 0;
	confhelp.title = "HELP";
	confhelp.key.f1_file = NULL;
	confhelp.key.f1_message = NULL;
	confhelp.button.with_help = false;
	for (;;) {
		output = bsddialog_mixedlist(&conf, text, h, w, 0, ngroups,
		    groups, &focuslist, &focusitem);
		if (output == BSDDIALOG_HELP) {
			output = bsddialog_textbox(&confhelp, helpfile, 0, 0);
			if(output == BSDDIALOG_ERROR)
				break;
			conf.button.default_label = "Help";
			bsddialog_clear(0);
		} else {
			break;
		}
	}
	bsddialog_end();
	if (output == BSDDIALOG_ERROR)
		fprintf(stderr, "Error: %s\n", bsddialog_geterror());
	if (output != BSDDIALOG_OK)
		return (EXIT_FAILURE);

	/* print selected items if OK */
	for (i = 0; i < ngroups; i++) {
		for (j = 0; j < groups[i].nitems; j++) {
			if (groups[i].type != BSDDIALOG_SEPARATOR) {
				if (groups[i].items[j].on)
					fprintf(stderr, "\"%s\" ",
					    groups[i].items[j].name);
			}
			free((char*)groups[i].items[j].name);
		}
		free(groups[i].items);
	}
	free(groups);

	return (EXIT_SUCCESS);
}
