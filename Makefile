# Any copyright is dedicated to the Public Domain, see:
#     <http://creativecommons.org/publicdomain/zero/1.0/>
#
# Written by Alfonso Sabato Siciliano

OUTPUT=  portconfig
SOURCES= portconfig.c theme.c
OBJECTS= ${SOURCES:.c=.o}

CFLAGS+= -I/usr/local/include -std=gnu99 -Wall -Wextra
LDFLAGS+= -L/usr/local/lib -Wl,-Bstatic -v -Wl,-whole-archive -lbsddialog \
	-Wl,-no-whole-archive -Wl,-Bdynamic -Wl,--export-dynamic -L/usr/lib \
	-lncursesw -ltinfow

# `make -DDEBUG`
.if defined(DEBUG)
CFLAGS+= -g -DDEBUG
.endif

# `make -DDEVELOPMENT`
.if defined(DEVELOPMENT)
LIBPATH= ${.CURDIR}/bsddialog/lib
CFLAGS= -g -I${LIBPATH} -std=gnu99 -Wall -Wextra -DDEBUG
LDFLAGS= -Wl,-rpath=${LIBPATH} -L${LIBPATH} -lbsddialog
.endif

all : ${OUTPUT}

${OUTPUT}: ${OBJECTS}
	${CC} ${LDFLAGS} ${OBJECTS} -o ${.PREFIX}

.c.o:
	${CC} ${CFLAGS} -c ${.IMPSRC} -o ${.TARGET}

clean:
	rm -f ${OUTPUT} *.o *~ *.core
