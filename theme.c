/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2024-2025 Alfonso Sabato Siciliano
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <bsddialog.h>
#include <bsddialog_theme.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define NPROPERTY  41
#define NCOLOR      8
#define NATTR       6
#define THEMEFILE "/usr/local/etc/portconfig/theme.conf"

struct bsddialog_theme th;

enum typeproperty {
	BOOL,
	CHAR,
	INT,
	UINT,
	COLOR,
	COMPAT
};

struct property {
	const char *comment;
	const char *name;
	enum typeproperty type;
	void *value;
};

struct namevalue {
	const char *name;
	unsigned int value;
};

static struct namevalue color[NCOLOR] = {
	{"black",   BSDDIALOG_BLACK},
	{"red",     BSDDIALOG_RED},
	{"green",   BSDDIALOG_GREEN},
	{"yellow",  BSDDIALOG_YELLOW},
	{"blue",    BSDDIALOG_BLUE},
	{"magenta", BSDDIALOG_MAGENTA},
	{"cyan",    BSDDIALOG_CYAN},
	{"white",   BSDDIALOG_WHITE}
};

static struct namevalue attr[NATTR] = {
 	{"bold",       BSDDIALOG_BOLD},
	{"reverse",    BSDDIALOG_REVERSE},
	{"underline",  BSDDIALOG_UNDERLINE},
	{"blink",      BSDDIALOG_BLINK},
	{"halfbright", BSDDIALOG_HALFBRIGHT},
	{"highlight",  BSDDIALOG_HIGHLIGHT}
};

#define PROP_ERROR(name, error) do {                                           \
	fclose(fp);                                                            \
	fprintf(stderr,"%s for \"%s\"", error, name);                          \
	return (bsddialog_set_default_theme(BSDDIALOG_THEME_FLAT));            \
} while (0)

static struct property p[NPROPERTY] = {
	{"\n#Terminal\n", "theme.screen.color", COLOR, &th.screen.color},

	{"\n# Shadow\n",
	    "theme.shadow.color", COLOR, &th.shadow.color},
	{"# shift down right from main widget\n",
	    "theme.shadow.y", UINT, &th.shadow.y},
	{"", "theme.shadow.x", UINT, &th.shadow.x},

	{"\n# Main widget\n",
	    "theme.dialog.color", COLOR, &th.dialog.color},
	{"", "theme.dialog.delimtitle", BOOL, &th.dialog.delimtitle},
	{"", "theme.dialog.titlecolor", COLOR, &th.dialog.titlecolor},
	{"", "theme.dialog.lineraisecolor", COLOR, &th.dialog.lineraisecolor},
	{"", "theme.dialog.linelowercolor", COLOR, &th.dialog.linelowercolor},
	{"", "theme.dialog.bottomtitlecolor", COLOR,
	    &th.dialog.bottomtitlecolor},
	{"", "theme.dialog.arrowcolor", COLOR, &th.dialog.arrowcolor},

	{"\n# Menus: --checklist, --menu, --radiolist\n"
	    "# prefix [selector] shortcut name desc bottomdesc\n",
	    "theme.menu.f_prefixcolor", COLOR, &th.menu.f_prefixcolor},
	{"", "theme.menu.prefixcolor", COLOR, &th.menu.prefixcolor},
	{"", "theme.menu.f_selectorcolor", COLOR, &th.menu.f_selectorcolor},
	{"", "theme.menu.selectorcolor", COLOR, &th.menu.selectorcolor},
	{"", "theme.menu.f_namecolor", COLOR, &th.menu.f_namecolor},
	{"", "theme.menu.namecolor", COLOR, &th.menu.namecolor},
	{"", "theme.menu.f_desccolor", COLOR, &th.menu.f_desccolor},
	{"", "theme.menu.desccolor", COLOR, &th.menu.desccolor},
	{"", "theme.menu.f_shortcutcolor", COLOR, &th.menu.f_shortcutcolor},
	{"", "theme.menu.shortcutcolor", COLOR, &th.menu.shortcutcolor},
	{"", "theme.menu.bottomdesccolor", COLOR, &th.menu.bottomdesccolor},
	{"# bsddialog_menutype BSDDIALOG_SEPARATOR\n",
	    "theme.menu.sepnamecolor", COLOR, &th.menu.sepnamecolor},
	{"", "theme.menu.sepdesccolor", COLOR, &th.menu.sepdesccolor},

	{"\n# Forms\n",
	    "theme.form.f_fieldcolor", COLOR, &th.form.f_fieldcolor},
	{"", "theme.form.fieldcolor", COLOR, &th.form.fieldcolor},
	{"", "theme.form.readonlycolor", COLOR, &th.form.readonlycolor},
	{"", "theme.form.bottomdesccolor", COLOR, &th.form.bottomdesccolor},

	{"\n# Bar of --gauge, --mixedgauge, --pause, --rangebox\n",
	    "theme.bar.f_color", COLOR, &th.bar.f_color},
	{"", "theme.bar.color", COLOR, &th.bar.color},

	{"\n# Buttons\n",
	    "theme.button.minmargin", UINT, &th.button.minmargin},
	{"", "theme.button.maxmargin", UINT, &th.button.maxmargin},
	{"", "theme.button.leftdelim", CHAR, &th.button.leftdelim},
	{"", "theme.button.rightdelim", CHAR, &th.button.rightdelim},
	{"", "theme.button.f_delimcolor", COLOR, &th.button.f_delimcolor},
	{"", "theme.button.delimcolor", COLOR, &th.button.delimcolor},
	{"", "theme.button.f_color", COLOR, &th.button.f_color},
	{"", "theme.button.color", COLOR, &th.button.color},
	{"", "theme.button.f_shortcutcolor", COLOR, &th.button.f_shortcutcolor},
	{"", "theme.button.shortcutcolor", COLOR, &th.button.shortcutcolor},

	{"\n#Compatibility. Do not use, can be deleted\n",
	    "use_shadow", COMPAT, NULL}
};

static int loadtheme(const char *file)
{
	bool boolvalue;
	char charvalue, *value;
	char line[BUFSIZ], name[BUFSIZ], c1[BUFSIZ], c2[BUFSIZ];
	int i, j, intvalue;
	unsigned int uintvalue, flags;
	enum bsddialog_color bg, fg;
	FILE *fp;


	if ((fp = fopen(file, "r")) == NULL) {
		printf("Cannot open theme \"%s\" file", file);
		return (bsddialog_set_default_theme(BSDDIALOG_THEME_FLAT));
	}

	while (fgets(line, BUFSIZ, fp) != NULL) {
		if (line[0] == '#' || line[0] == '\n')
			continue;  /* superfluous, only for efficiency */
		sscanf(line, "%s", name);
		value = NULL; /* useless init, fix compiler warning */
		for (i = 0; i < NPROPERTY; i++) {
			if (strcmp(name, p[i].name) == 0) {
				value = &line[strlen(name)];
				break;
			}
		}
		if (i >= NPROPERTY) {
			/* unknown name in property p[] */
			if (strcmp(name, "version") == 0)
				continue; /* nothing for now */
			else
				PROP_ERROR(name, "Unknown theme property name");
		}
		switch (p[i].type) {
		case CHAR:
			while (value[0] == ' ' || value[0] == '\n' ||
			    value[0] == '\0')
				value++;
			if (sscanf(value, "%c", &charvalue) != 1)
				PROP_ERROR(p[i].name, "Cannot get a char");
			*((int*)p[i].value) = charvalue;
			break;
		case INT:
			if (sscanf(value, "%d", &intvalue) != 1)
				PROP_ERROR(p[i].name, "Cannot get a int");
			*((int*)p[i].value) = intvalue;
			break;
		case UINT:
			if (sscanf(value, "%u", &uintvalue) != 1)
				PROP_ERROR(p[i].name, "Cannot get a uint");
			*((unsigned int*)p[i].value) = uintvalue;
			break;
		case BOOL:
			boolvalue = (strstr(value, "true") != NULL) ?
			    true : false;
			*((bool*)p[i].value) = boolvalue;
			break;
		case COLOR:
			if (sscanf(value, "%s %s", c1, c2) != 2)
				PROP_ERROR(p[i].name, "Cannot get 2 colors");
			/* Foreground */
			for (j = 0; j < NCOLOR ; j++)
				if ((strstr(c1, color[j].name)) != NULL)
					break;
			if (j >= NCOLOR)
				PROP_ERROR(p[i].name, "Bad foreground");
			fg = color[j].value;
			/* Background */
			for (j = 0; j < NCOLOR ; j++)
				if ((strstr(c2, color[j].name)) != NULL)
					break;
			if (j >= NCOLOR)
				PROP_ERROR(p[i].name, "Bad background");
			bg = color[j].value;
			/* Flags */
			flags = 0;
			for (j = 0; j < NATTR; j++)
				if (strstr(value, attr[j].name) != NULL)
					flags |= attr[j].value;
			*((int*)p[i].value) = bsddialog_color(fg, bg, flags);
			break;
		case COMPAT:
			/*
			 * usr.sbin/bsdconfig/share/dialog.subr:2255
			 * uses this parameter to set NO_SHADOW.
			 * Set t.shadow.[y|x] for compatibilty.
			 */
			if (strcmp(name, "use_shadow") == 0) {
				 if (strcasestr(value, "off") != NULL)
				 	th.shadow.y = th.shadow.x = 0;
			}
			break;
		}
	}

	fclose(fp);

	if (bsddialog_set_theme(&th) != BSDDIALOG_OK)
		return (bsddialog_set_default_theme(BSDDIALOG_THEME_FLAT));

	return (BSDDIALOG_OK);
}

int set_theme(void)
{
	char *env;
	int rv;

	/* Init bsddialog */
	if ((rv = bsddialog_init_notheme()) == BSDDIALOG_ERROR)
		; /* nothing, return error */
	/* Does term provide colors? */
	else if (bsddialog_hascolors() == false)
		rv = bsddialog_set_default_theme(BSDDIALOG_THEME_BLACKWHITE);
	/* env NO_COLOR */
	else if ((env = getenv("NO_COLOR")) != NULL && env[0] != '\0')
		rv = bsddialog_set_default_theme(BSDDIALOG_THEME_BLACKWHITE);
	/* env PORTCONFIG_THEMEFILE=<file> */
	else if ((env = getenv("PORTCONFIG_THEMEFILE")) != NULL) {
		if (access(env, F_OK) == 0)
			rv = loadtheme(env);
		else
			rv = bsddialog_set_default_theme(BSDDIALOG_THEME_FLAT);
	}
	/* /usr/local/etc/portconfig/theme.conf */
	else if (access(THEMEFILE, F_OK) == 0)
		rv = loadtheme(THEMEFILE);
	/* Default bsddialog theme */
	else
		rv = bsddialog_set_default_theme(BSDDIALOG_THEME_FLAT);

	return (rv);
}
